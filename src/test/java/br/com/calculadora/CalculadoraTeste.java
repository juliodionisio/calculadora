package br.com.calculadora;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestTemplate;

public class CalculadoraTeste {

    private Calculadora calculadora;

    @BeforeEach
    public void setUp(){
        calculadora = new Calculadora();
    }

    @Test
    public void testaASomaDeDoisNumerosInteiros(){
        int resultado = calculadora.soma(1, 2);

        Assertions.assertEquals(3, resultado);
    }

    @Test
    public void testaASomaDeDoisNumerosFlutuantes(){
        double resultado = calculadora.soma(2.3, 3.4);

        Assertions.assertEquals(5.7, resultado);
    }

    @Test
    public void testaDivisaoDeDoisNumerosPositivos(){
        double resultado = calculadora.divisao(10, 2);

        Assertions.assertEquals( 5, resultado);
    }

    @Test
    public void testaADivisaoDeDoisNumerosNegativos(){
        double resultado = calculadora.divisao(5, -10);

        Assertions.assertEquals( -0.5, resultado);
    }

    @Test
    public void testaADivisaoDeDoisNumerosFlutuantes(){
        double resultado = calculadora.divisao(6.6, 5.5);

        Assertions.assertEquals( 1.2, resultado);
    }

    @Test
    public void testaAMultiplicacaoDeDoisNumerosPositivos(){
        double resultado = calculadora.multiplicacao(10, 4);

        Assertions.assertEquals( 40, resultado);
    }

    @Test
    public void testaAMultiplicacaoDeDoisNumerosNegativos(){
        double resultado = calculadora.multiplicacao(-10, -2);

        Assertions.assertEquals( 20, resultado);

    }

    @Test
    public void testaAMultiplicacaoDeDoisNumerosFlutuantes(){
        double resultado = calculadora.multiplicacao(3.3, 8.7);

        Assertions.assertEquals( 28.71, resultado);
    }

}
