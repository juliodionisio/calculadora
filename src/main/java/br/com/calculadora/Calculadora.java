package br.com.calculadora;

import java.math.BigDecimal;
import java.math.RoundingMode;

public class Calculadora {

    public Calculadora(){

    }

    public int soma(int primeiroNumero, int segundoNumero){
        int resultado = primeiroNumero + segundoNumero;
        return resultado;
    }

    public double soma(double primeiroNumero, double segundoNumero){
        double resultado = primeiroNumero + segundoNumero;

        BigDecimal bigDecimal = new BigDecimal(resultado).setScale(3 , RoundingMode.HALF_EVEN);
        return bigDecimal.doubleValue();
    }

    public double divisao(double primeiroNumero, double segundoNumero){
        double resultado = primeiroNumero/segundoNumero;
        BigDecimal bigDecimal = new BigDecimal(resultado).setScale(3 , RoundingMode.HALF_EVEN);
        return bigDecimal.doubleValue();

    }

    public double multiplicacao(double primeiroNumero, double segundoNumero){
        double resultado = primeiroNumero*segundoNumero;
        BigDecimal bigDecimal = new BigDecimal(resultado).setScale(3 , RoundingMode.HALF_EVEN);
        return bigDecimal.doubleValue();
    }
}
